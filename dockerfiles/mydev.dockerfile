# hadolint ignore=DL3007
FROM docker.io/archlinux:latest

RUN : \
    && pacman -Syyu --noconfirm \
    && pacman -S --noconfirm \
        base-devel \
        git \
        jq \
        pacman-contrib \
        rsync \
    # apply my dotfiles
    && curl 'https://dotfiles.glatan.vercel.app' > install.sh \
    && bash install.sh --apply-root \
    # Clear cache
    && rm -rf \
        /usr/share/doc/* \
        /usr/share/man/* \
        /var/cache/pacman/pkg/* \
        /var/lib/pacman/sync/* \
        install.sh

CMD ["/usr/bin/bash"]
